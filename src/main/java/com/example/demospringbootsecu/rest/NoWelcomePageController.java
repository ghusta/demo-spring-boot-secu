package com.example.demospringbootsecu.rest;

import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * See profile activation on <a href="https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.profiles">profiles documentation</a>.
 */
@RestController
@Profile("no-welcome-page")
public class NoWelcomePageController {

    /**
     * This endpoint <b>overrides</b> the default welcome page (index.html) when enabled.
     */
    @GetMapping("/")
    public String homePage() {
        return "Hello ! 🐣";
    }

}
