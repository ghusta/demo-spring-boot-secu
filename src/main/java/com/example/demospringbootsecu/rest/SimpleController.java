package com.example.demospringbootsecu.rest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class SimpleController {

    @GetMapping("/public")
    public String publicPage() {
        return "Hello to anybody !!! 😸";
    }

    @GetMapping("/private")
    public String privatePage(@AuthenticationPrincipal UserDetails principal) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Objects.requireNonNull(authentication);

        String name = principal.getUsername();
        return "Attention ! 🔑 " + (name == null ? "" : "-- " + name);
    }

    @GetMapping("/code/{code}")
    public String getCode(@AuthenticationPrincipal UserDetails principal, @PathVariable String code) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Objects.requireNonNull(authentication);

        String name = principal.getUsername();
        return "🔑 Code is  : " + code;
    }

    @GetMapping("/dont-call-me")
    @PreAuthorize("denyAll")
    public String deprecatedMethod() {
        return "OK";
    }

}
