package com.example.demospringbootsecu.security;

import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.config.annotation.web.configurers.ExceptionHandlingConfigurer;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;

public class MyCustomConfigurer extends AbstractHttpConfigurer<MyCustomConfigurer, HttpSecurity> {

    @Override
    public void init(HttpSecurity http) throws Exception {
        System.out.println("-".repeat(60));
        System.out.println("Entrée : " + this.getClass().getSimpleName() + "#init");

        http.exceptionHandling()
                .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));

//        http.authorizeRequests(authz -> authz
//                .expressionHandler(new DefaultWebSecurityExpressionHandler()));
        // --> java.lang.IllegalStateException: At least one mapping is required (i.e. authorizeRequests().anyRequest().authenticated())

        ApplicationContext context = http.getSharedObject(ApplicationContext.class);
        System.out.println("has shared ApplicationContext ? " + (context != null));

        AuthenticationManager authenticationManager = http.getSharedObject(AuthenticationManager.class);
        System.out.println("has authenticationManager ? " + (authenticationManager != null));

        UserDetailsService userDetailsService = http.getSharedObject(UserDetailsService.class);
        System.out.println("has userDetailsService ? " + (userDetailsService != null));

        AuthenticationEntryPoint authenticationEntryPoint = http.getSharedObject(AuthenticationEntryPoint.class);
        System.out.println("has shared AuthenticationEntryPoint ? " + (authenticationEntryPoint != null));

        ExceptionHandlingConfigurer<?> exceptionConf = http.getConfigurer(ExceptionHandlingConfigurer.class);
        System.out.println("has configurer ExceptionHandlingConfigurer ? " + (exceptionConf != null));

        AuthorizeHttpRequestsConfigurer authorizeHttpRequestsConfigurer = http.getConfigurer(AuthorizeHttpRequestsConfigurer.class);
        System.out.println("has configurer AuthorizeHttpRequestsConfigurer ? " + (authorizeHttpRequestsConfigurer != null));

        ExpressionUrlAuthorizationConfigurer expressionUrlAuthorizationConfigurer = http.getConfigurer(ExpressionUrlAuthorizationConfigurer.class);
        System.out.println("has configurer ExpressionUrlAuthorizationConfigurer ? " + (expressionUrlAuthorizationConfigurer != null));
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        System.out.println("-".repeat(60));
        System.out.println("Entrée : " + this.getClass().getSimpleName() + "#configure");

        ApplicationContext context = http.getSharedObject(ApplicationContext.class);
        System.out.println("has shared ApplicationContext ? " + (context != null));

        Environment env = context.getEnvironment();

        AuthenticationManager authenticationManager = http.getSharedObject(AuthenticationManager.class);
        System.out.println("has authenticationManager ? " + (authenticationManager != null));

        UserDetailsService userDetailsService = http.getSharedObject(UserDetailsService.class);
        System.out.println("has userDetailsService ? " + (userDetailsService != null));

        AuthenticationEntryPoint authenticationEntryPoint = http.getSharedObject(AuthenticationEntryPoint.class);
        System.out.println("has shared AuthenticationEntryPoint ? " + (authenticationEntryPoint != null));

        ExceptionHandlingConfigurer<?> exceptionConf = http.getConfigurer(ExceptionHandlingConfigurer.class);
        System.out.println("has configurer ExceptionHandlingConfigurer ? " + (exceptionConf != null));

        AuthorizeHttpRequestsConfigurer authorizeHttpRequestsConfigurer = http.getConfigurer(AuthorizeHttpRequestsConfigurer.class);
        System.out.println("has configurer AuthorizeHttpRequestsConfigurer ? " + (authorizeHttpRequestsConfigurer != null));

        ExpressionUrlAuthorizationConfigurer expressionUrlAuthorizationConfigurer = http.getConfigurer(ExpressionUrlAuthorizationConfigurer.class);
        System.out.println("has configurer ExpressionUrlAuthorizationConfigurer ? " + (expressionUrlAuthorizationConfigurer != null));

//        http.addFilter(new CustomFilter(authenticationManager));
    }

    public static MyCustomConfigurer customDsl() {
        return new MyCustomConfigurer();
    }

}