package com.example.demospringbootsecu;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import java.util.Collections;

import static com.example.demospringbootsecu.security.MyCustomConfigurer.customDsl;
import static org.springframework.security.config.Customizer.withDefaults;

@Configuration(proxyBeanMethods = false)
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfiguration {

    /**
     * Possible welcome page addition from Spring Boot.
     *
     * @see org.springframework.boot.autoconfigure.web.servlet.WelcomePageHandlerMapping
     */
    private static final String[] WELCOME_PAGE = new String[]{
            "/",
    };

    private static final String[] ANGULAR_RESOURCES = new String[]{
            "/index.html",
            "/favicon.ico", "/favicon*", "/site.webmanifest", "/apple-touch-icon.png",
            "/styles.*.css",
            "/main.*.js",
            "/runtime.*.js",
            "/polyfills.*.js",
    };

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
                .apply(customDsl())
                .and()

                .authorizeHttpRequests(authz -> authz
                        .requestMatchers(WELCOME_PAGE).permitAll()
                        .requestMatchers(ANGULAR_RESOURCES).permitAll()
                        .requestMatchers("/public").permitAll()
                        .requestMatchers("/private").authenticated()
//                        .mvcMatchers("/code/{id}").access(SecurityConfiguration::checkCode)
//                        .requestMatchers("/special").access(new WebExpressionAuthorizationManager(" hasRole('TOTO') "))
                        .requestMatchers("/special2").access((authentication, object) -> alwaysGranted())
//                        .requestMatchers("/special3").access(xxx)
                        .requestMatchers("/role-x").hasRole("X")
                        .anyRequest().denyAll())

//                .formLogin(withDefaults())
                .httpBasic(withDefaults())
                .x509(withDefaults())
                .oauth2ResourceServer(AbstractHttpConfigurer::disable)
                .build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new InMemoryUserDetailsManager(
                User.builder()
                        .username("user").password("{noop}pwd")
                        .authorities(Collections.emptyList())
                        .build(),
                User.builder()
                        .username("toto").password("{noop}pwd2")
                        .roles("BIDON")
                        .build()
        );
    }

    private static AuthorizationDecision alwaysGranted() {
        return new AuthorizationDecision(true);
    }

}
