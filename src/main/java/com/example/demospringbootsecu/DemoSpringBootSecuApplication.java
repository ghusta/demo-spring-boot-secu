package com.example.demospringbootsecu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringBootSecuApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringBootSecuApplication.class, args);
	}

}
